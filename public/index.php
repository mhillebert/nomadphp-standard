<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('html_errors', 1);
/**
 * Index.php
 *
 * @author Mark Hillebert
 * @package NomadPhp
 */
//define some folder names
const APPLICATION_DIR_NAME = "Application";
const VENDOR_DIR_NAME = "vendor";
const TEMP_DIR_NAME = 'tmp';

//define some paths
defined("PUBLIC_ROOT") or define("PUBLIC_ROOT", realpath(__DIR__));
defined("APPLICATION_ROOT") or define("APPLICATION_ROOT", str_ireplace(basename(PUBLIC_ROOT), APPLICATION_DIR_NAME, PUBLIC_ROOT));
defined("PACKAGE_DIRECTORY") or define ("PACKAGE_DIRECTORY", 'package');
defined("VENDOR_ROOT") or define("VENDOR_ROOT", str_ireplace(basename(PUBLIC_ROOT), VENDOR_DIR_NAME, PUBLIC_ROOT));
defined("TEMP_DIR") or define("TEMP_DIR", str_ireplace(basename(PUBLIC_ROOT), TEMP_DIR_NAME, PUBLIC_ROOT));
defined("HOST_NAME") or define("HOST_NAME", $_SERVER['HTTP_HOST']);
require(VENDOR_ROOT . DIRECTORY_SEPARATOR . 'autoload.php');

$app = new \Nomad\Application();
$app->nomadApplicationExecutor();
//require(APPLICATION_ROOT . DIRECTORY_SEPARATOR . 'Bootstrap.php');

