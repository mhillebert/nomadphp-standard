{"hash":"02397d77172810aca530c14947712ab0"}
<?php
/**
 * Always start a .css.php file on line 2 because the above hash is generated just to detect changes.
 * The {filename}.css.php will generate a {filename}.css file in the {themeName}/assets/css/ directory. This file
 * will then be copied to the public folder along with the other assets; but only for `development` environments.
 *
 * This is an example file. Here you can use any Xcss functions; e.g, $color->shade(), $asset->image() etc.
 */
?>

<?php
	$mainColor = "#FBB040";
	$secondaryColor = "#337ab7";
	$tertiaryColor = "#BF3E0A";
?>
body {
	background-size     : cover;
	background-position : bottom;
	font-family         : Tahoma, Geneva, sans-serif;
	min-height          : 100vh;
}

section:first-child {
	background          : <?= $asset->image('nomadbw.jpg');?> no-repeat;
	background-size     : cover;
	background-position : bottom;
}

section {
	min-height : 100vh;
}

section:not(:first-child) {
	min-height : calc(100vh - 5rem);
}

section:first-child p, section:first-child h1 {
	color       : #ffffff;
	text-shadow : 3px 3px 5px black;
}
section:first-child h1 { font-size:5rem;}
section:first-child p { font-size:2.5rem;}
section:first-child .col-sm-6:first-child {
	margin-top   : 12%;
	padding-left : 14%;
}

section:first-child div.row:nth-child(2) {
	color       : #ffffff;
	position    : absolute;
	bottom      : 0;
	padding     : .5rem;
	width       : 100%;
	font-family : Cabin, serif;
	overflow    : hidden;
	background : linear-gradient(to right, rgba(0,0,0,0), <?=$color->rgba($mainColor, .65)?>, rgba(0,0,0,0));
}

section:first-child div.row:nth-child(2) > div > div {
	display    : inline-block;
	height     : 5rem;
	bottom     : 1rem;
}

#disclaimer {
	background : #383838;
	color      : #ffffff;
	font-style : italic;
	padding    : .25rem;
}
nav button{
	transition: background-color 500ms;
	font-family: serif;
}
nav ul {
	list-style : none;
	padding:0;
}

nav li {
	display : inline-block;
}

nav li {
	margin : .5rem 3vw;
	border-radius: 5px;
	border: 1px solid #ffffff;
	min-width: 13rem;
	padding:.5rem;
	transition: background-color 500ms;
	font-size:1.6rem !important;
}
nav li a:hover {text-decoration: none;}
nav li:hover{ background: rgba(255,255,255,.25);}
#side-nav{min-height:100%;height:100vh;background-color:<?= $secondaryColor?>;text-align:center;}
#side-nav img{margin:5vh auto;position:relative;}
#side-nav img.smaller{margin: 3vh auto 1vh; max-width: 18vh;}
#side-nav li{text-align:left;width:100%;margin:0;display:block;}
#side-nav li.active{background-color:rgba(255,255,255, .25);}
#side-nav a {color:#ffffff;}
.row-eq-height{
	display:table;
}
.row-eq-height > div[class*='col-'] {
display: table-cell;
	vertical-align: top;
}
.equal{
	float:none;
}