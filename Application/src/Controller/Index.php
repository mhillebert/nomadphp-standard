<?php
namespace Application\Controller;

/**
 * Class Index
 *
 * @package Application\Controller
 */
class Index extends \Nomad\Core\Controller
{
	/**
	 * @Nomad\Layout singleColumn
	 */
	public function index()
	{
		/**
		 * Examples of how to set variables into the view. Variables can be of any type.
		 * Notice, these are being used in header/footer blocks; however, the index.phtml has access too.
		 */
		$this->view->slogan = "Conveniently full-featured &amp; powerful, yet lightweight and unrefined.";

	}

	/**
	 * @Nomad\Layout singleColumn
	 */
	public function about()
	{
		$this->view->hello = "Hello";

	}
}