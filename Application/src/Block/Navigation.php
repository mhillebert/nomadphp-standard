<?php
namespace Application\Block;

use Nomad\Core\Viewable;

class Navigation
	extends \Nomad\Container\Html\Navigation
{

	/**
	 * Primary navigation
	 *
	 * @throws \Nomad\Exception\Container
	 */
	public function primary($options = null)
	{
		$isSideNav = $options && isset($options['side-navigation']) && $options['side-navigation'] === true;
		$nav       = new \Nomad\Container\Html\Navigation();
		$nav->addClass('primary-navigation');

		//About
		$about = array();
		if ($isSideNav) {
			$about = ['label' => "<i class='glyphicon glyphicon-info-sign'></i> <span>About</span>"];
		}
		$nav->addLink(
			array_merge(
				array(
					'label'      => 'About',
					'href'       => '/about-nomadphp',
					'attributes' => array(
						'class' => 'btn'
					)
				),
				$about
			));

		//Get started
		$getStarted = array();
		if ($isSideNav) {
			$getStarted = array(
				'href'       => '/#2',
				'tag'        => 'li',
				'attributes' => ['class' => 'btn'],
			);
		}
		$nav->addLink(
			array_merge(
				array(
					'label'      => '<i class="glyphicon glyphicon-arrow-down"></i> <span>Get Started Now!</span>',
					'href'       => '#2',
					'tag'        => 'button',
					'attributes' => array(
						'class' => 'btn btn-lg btn-primary'
					)
				),
				$getStarted
			));

		//Docs
		$docs = array();
		if ($isSideNav) {
			$docs = array("label" => "<i class='glyphicon glyphicon-book'></i> <span>Documentation</span>");
		}
		$nav->addLink(
			array_merge(
				array(
					'identifier' => 'docs',
					'label'      => 'Documentation',
					'href'       => '/documentation',
					'attributes' => array(
						'class' => 'btn',
						'id' => 'documentation'
					)
				),
				$docs
			));
		if ($options && isset($options['append']) && $options['append'] === true) {
			$nav->addSubMenuTo('docs', ['menu' => $this->documentation()]);
		}
		return $nav->render();
	}

	public function documentation()
	{
		$nav       = array();
		$nav[]=
				array(
					'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Routing</span>',
					'href'       => '/documentation/routing',

			);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Annotations</span>',
				'href'       => '/documentation/annotations',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Registry</span>',
				'href'       => '/documentation/registry',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Sessions</span>',
				'href'       => '/documentation/sessions',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Cache</span>',
				'href'       => '/documentation/cache',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Forms</span>',
				'href'       => '/documentation/forms',

		);

		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Views</span>',
				'href'       => '/documentation/views',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Themes</span>',
				'href'       => '/documentation/themes',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Database</span>',
				'href'       => '/documentation/database',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Models</span>',
				'href'       => '/documentation/models',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Emails</span>',
				'href'       => '/documentation/emails',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Access Control</span>',
				'href'       => '/documentation/access-control',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>XCss</span>',
				'href'       => '/documentation/xcss',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Static Helpers</span>',
				'href'       => '/documentation/static-helpers',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Filters</span>',
				'href'       => '/documentation/filters',

		);
		$nav[]=
			array(
				'label'      => '<i class="glyphicon glyphicon-menu-right"></i> <span>Validators</span>',
				'href'       => '/documentation/validators',

		);

		return $nav;
	}
}